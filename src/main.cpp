/*
* i2c_port_address_scanner
* Scans ports D0 to D7 on an ESP8266 and searches for I2C device. based on the
* original code available on Arduino.cc and later improved by user Krodal and Nick Gammon
* (www.gammon.com.au/forum/?id=10896)
* D8 throws exceptions thus it has been left out
*/

#include <Arduino.h>
#include <Wire.h>

byte adresses[127];
uint8_t portArray[] = {16, 5, 4, 0, 2, 14, 12, 13};
String portMap[] = {"D0", "D1", "D2", "D3", "D4", "D5", "D6", "D7"}; // for
                                                                     // Wemos
// String portMap[] = {"GPIO16", "GPIO5", "GPIO4", "GPIO0", "GPIO2", "GPIO14", "GPIO12", "GPIO13"};

int check_if_exist_I2C() {
  byte error, address;
  int nDevices = 0;

  for (address = 1; address < 127; address++) {
    // The i2c_scanner uses the return value of
    // the Write.endTransmisstion to see if
    // a device did acknowledge to the address.
    Wire.beginTransmission(address);
    error = Wire.endTransmission();

    if (error == 0) {
      adresses[nDevices] = address;
      nDevices++;
//    } else if (error == 4) {
//      // Serial.print("Unknow error at address 0x");
    }
  }
  return nDevices;
}

void scanPorts() {
  int nDevices = 0;
  for (uint8_t i = 0; i < sizeof(portArray); i++) {
    for (uint8_t j = 0; j < sizeof(portArray); j++) {
      if (i != j) {
        Wire.begin(portArray[i], portArray[j]);
        nDevices = check_if_exist_I2C();
        if (nDevices > 0) {
          Serial.printf("SDA %s (%2d); SCL %s (%2d): ", portMap[i].c_str(), i,
                        portMap[j].c_str(), j);
          for (int d = 0; d < nDevices; d++) {
            Serial.printf("%03x ", adresses[d]);
          }
          Serial.print("\n");
        }
      }
    }
  }
}

void setup() {
  Serial.begin(115200);
  while (!Serial)
    ; // Leonardo: wait for serial monitor
  Serial.println(
      "\n\nI2C Scanner to scan for devices on each port pair D0 to D7");
  scanPorts();
}

void loop() {}